package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class AnimalCategoriesReader extends CsvReader {
	protected final List<String> lines;
    private final Path file;
    private List<String> animalCategory = new ArrayList<String>();
	public AnimalCategoriesReader(Path file) throws IOException {
        super(file);
    }

    public List<String> getAnimalCategory() {
        return animalCategory;
    }

    public long countValidRecords(){
        for(String record : lines){
            String[] data = record.split(COMMA);
            try{
                for(String categorie : animalCategory){
                    if(data[1].equals(categorie)){
                        throw new Exception();
                    }
                }
                animalCategory.add(data[1]);
            }
            catch(Exception e){
                continue;
            }

        }

        return (long) animalCategory.size();
    }


    public long countInvalidRecords(){
        long invalid = 0;
        for (String recorded : lines) {
            String[] data = recorded.split(:COMMA);
            if (data.length < 2){
                invalid++;
            }
        }
        return invalid;
    }
    //implements here

}