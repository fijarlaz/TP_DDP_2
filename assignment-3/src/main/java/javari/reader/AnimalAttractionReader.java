package javari.reader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
public class AnimalAttractionReader extends CsvReader {
    private List<String> atraksi = new ArrayList<String>();
    public AnimalAttractionReader(Path file){
        super(file);
    }
    public long countValidRecords() {
        for (String recorded : lines) {
            String[] lineOfString = recorded.split(COMMA;
            try {
                for (String aksi : atraksi){
                    if (lineOfString[1].equalsIgnoreCase(aksi)){
                        throw new Exception();
                    }
                }
                atraksi.add(lineOfString[1]);
            }
            catch (Exception e){

            }
        }
        return (long)atraksi.size();
    }
    public long countInvalidRecords(){
        long invalidCounter = 0;
        for (String isiLine:lines) {
            String[] lineOfString = isiLine.split(COMMA);
            if (lineOfString.length!=2){
                invalidCounter++;
            }
        }
        return invalidCounter;
    }
}
