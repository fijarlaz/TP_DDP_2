package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class AnimalRecords extends CsvReader {
    private List<String>  = new ArrayList<Animal>();
    private final Path file;
    protected final List<String> lines;
    public AnimalRecords(Path file) throws IOException {
        super(file);
    }

    public abstract long countValidRecords(){
        long validCounter =0;
        for (String recorded : lines) {
            String[] stringPerLine = recorded.split(COMMA);
            if (stringPerLine.length==8){
                validCounter +=1;
            }
        }
        return validCounter;
    }

    public long countInvalidRecords(){
        long invalid = 0;
        for (String recorded : lines){
            String[] stringLine = recorded.split(COMMA);
            if (stringLine.length!=8){
                invalid++;
            }
        }
        return invalid;
    }
}