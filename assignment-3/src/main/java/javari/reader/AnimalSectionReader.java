package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class AnimalSectionReader extends CsvReader {
    private List<String> section = new ArrayList<String>();
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public AnimalSectionReader(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        for (String recorded : lines) {
            String[] lineOfString = recorded.split(COMMA);
            try{
                for (String a : section){
                    if (data[2].equalsIgnoreCase(a)){
                        throw new Exception();
                    }
                }
                section.add(data[2])
            }
            catch (Exception e){

            }
        }
        return (long) section.size();
    }

    @Override
    public long countInvalidRecords() {
        long invalidCounter = 0;
        for (String recorded : lines){
            String[] lineOfString = recorded.split(COMMA);
            if (lineOfString.length!=3){
                invalidCounter++;
            }
        }
        return invalidCounter;
    }
}
