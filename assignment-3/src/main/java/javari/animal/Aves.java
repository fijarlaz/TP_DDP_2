import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

class Aves extends Animal {
    public boolean isLayingEgg = false;
	public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight,String layingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (layingEggs.equalsIgnoreCase("laying eggs")){
            isLayingEgg = true;
        }
    }

    public boolean specificCondition(){
        return !(this.isLayingEgg());
    }
}
