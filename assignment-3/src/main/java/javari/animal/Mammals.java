import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

class Mammals extends Animal {
    public static String kategori = "Explore the Mammals";
    public static String[] = {"Hamster","Lion","Cat","Eagle","Parrot","Snake","Whale"}

    public boolean isPregnant = false;
	public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight,String pregant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (pregant.equalsIgnoreCase("pregnant")){
            this.isPregnant = true;
        }
    }



	public boolean specificCondition(){
		if (isPregnant){
		    return false;
        }
        else return !this.getType().equalsIgnoreCase("lion") || this.getGender() != Gender.FEMALE;
    }
}
