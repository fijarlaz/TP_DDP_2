import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

class Reptiles extends Animal {
    public boolean isTame = false;
	public Reptiles(Integer id, String type, String name, Gender gender, double length,
                     double weight,String tamed, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (tamed.equalsIgnoreCase("tamed")){
            isTame = true;
        }
    }

    @Override
    public boolean isShowable() {
        return !(isTame);
    }
}