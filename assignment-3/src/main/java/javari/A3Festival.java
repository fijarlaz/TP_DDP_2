import javari.reader.*;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import javari.writer.*;
import java.nio.file.Paths;
import java.util.List;
public class A3Festival{
    boolean chooseSection = true;
    boolean chooseAnimalType = false;
    boolean chooseAttraction = false;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();

        System.out.println("Please provide the source data path: ");
        String filePath = input.nextLine();
        try{
            AnimalCategoriesReader animalCategories = new AnimalCategoriesReader(Paths.get(filePath+"\\animals_categories.csv"));
            AnimalAttractionReader attractionLine = new AnimalAttractionReader(Paths.get(filePath+"\\animals_attractions"));
            AnimalRecords animalData = new AnimalRecords(Paths.get(filePath+"\\animals_records.csv"));
        }
        catch (IOException e) {
            System.out.println("File not found or incorrect file!");
            System.out.println();
            System.exit(0);
        }

        System.out.println();
        System.out.println("... Loading... Success... System is populating data...\n");
        //implement yang found found tea

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        //implemets pilih jenis input
        System.out.println("Wow, one more step,\nplease let us know your name: ");
        String name = input.nextLine();
        System.out.println();
        System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:");
        //code untuk nulis data visitor
        System.out.println("Is the data correct? (Y/N): ");
        String jawaban = input.nextLine();
        if (jawaban.equalsIgnoreCase("Y")) {
            System.out.println("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
            String jawaban2 = input.nextLine();
            if (jawaban2.equalsIgnoreCase("N")) {
                System.out.println("\n... End of program, write to .json ..."); //file json
                RegistrationWriter.writeJson();
            }
        }
    }
}