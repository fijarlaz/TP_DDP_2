package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;


public class Attraction implements SelectedAttraction{
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();

    public Attraction(String name, String type){
        this.name = name;
        this.type = type;
    }

	public String getName(){
		//return name of attraction
		return name;
	}

	public String getType(){
		//return type of animals that perform this attraction
		return type;
	}

	public List<Animal> getPerformers(){
        return performers;
	}

	public boolean addPerformer(Animal performer){
	    if (performer.isShowable() && performer.getType().equalsIgnoreCase(type)){
	        performers.add(performer);
        }
    }

}