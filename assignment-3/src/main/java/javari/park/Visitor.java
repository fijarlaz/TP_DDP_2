package park;


import javari.park.Registration;
import javari.park.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private String visitorName;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<SelectedAttraction>();
    private int registrationId;

	public int getRegistrationId(){
        return registrationId;
	}


    public String getVisitorName(){
	    return visitorName;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return
     */
    public String setVisitorName(String name){
        this.visitorName = name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    public List<SelectedAttraction> getSelectedAttractions(){
        return selectedAttractions;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return {@code true} if the attraction is successfully added into the
     *     list, {@code false} otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected){
        if (selected!= null){
            selectedAttractions.add(selected);
            return true;
        }
        return false;
    }
}