import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



import javax.swing.*;

public class GameListener implements ActionListener {
    private MemoryPanel panel;
    private JButton tempButton;
    private static Card lastCard;
    private Card currentCard;

    public GameListener(MemoryPanel panel){
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        tempButton = (JButton) e.getSource();
        int stepCounter = panel.getStepCounter();
        currentCard = panel.findCard(tempButton);
        currentCard.setClicked(true);
        if (stepCounter == 0) runCaseOne();
        else if (stepCounter == 1) runCaseTwo();
        else if (stepCounter == 2) closeOpenedCard();
        currentCard.setClicked(false);
        checkWin();
    }

    private void closeOpenedCard() {
        for (Card card : panel.getcardList()){
            if (!card.isCorrect()) card.closeCard();
        }
        panel.resetStepCounter();

    }

    public void checkWin() {
        boolean win = true;
        for (Card c : panel.getcardList()) {
            win = win & c.isCorrect();
        }
        if (win) {
            panel.win();
        }
    }
    private void runCaseOne(){
        if (currentCard.isClicked()) currentCard.openCard();
        lastCard = currentCard;
        currentCard.setClicked(false);
        lastCard.setClicked(false);
        panel.addStepCounter();
    }

    private void runCaseTwo(){
        if (currentCard.isClicked()) currentCard.openCard();
        if (currentCard.getButton().equals(lastCard.getButton())) {
            currentCard.closeCard();
            currentCard.setClicked(false);
            panel.resetStepCounter();
            panel.addCounter();
            return;
        }
        if (lastCard.equals(currentCard)) {
            matchedCard();
            return;
        }
        currentCard.setClicked(false);
        lastCard.setClicked(false);
        panel.addStepCounter();
        panel.addCounter();
    }

    private void matchedCard(){
        lastCard.getButton().setEnabled(false);
        lastCard.setCorrect(true);
        currentCard.getButton().setEnabled(false);
        currentCard.setCorrect(true);
        panel.addCounter();
        panel.resetStepCounter();
    }

}
