import javax.swing.*;

public class MemoryGame {
    public MemoryGame(){
        JFrame frame = new JFrame("Ingat lambang timnya awiawi mbeeek");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(740, 1000);
        MemoryPanel panel = new MemoryPanel();
        frame.getContentPane().add(panel.getMainPanel());
        frame.setResizable(false);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MemoryGame game = new MemoryGame();
            }
        });
    }
}
