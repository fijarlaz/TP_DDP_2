import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class MemoryPanel {
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JLabel tryLabel;
    private JLabel highLabel;
    private int totalCounter;
    private int lowestRecord;
    private int stepCounter;
    private ArrayList<Card> cardList = new ArrayList<>();
    private GameListener command = new GameListener(this);

    public MemoryPanel(){
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        buttonPanel = createCards();
        mainPanel.add(buttonPanel, BorderLayout.PAGE_START);
        tryLabel = new JLabel("Banyak percobaan: " + totalCounter);
        mainPanel.add(tryLabel, BorderLayout.WEST);
        highLabel = new JLabel("         Rekor sejauh ini: " + lowestRecord);
        mainPanel.add(highLabel, BorderLayout.CENTER);
        mainPanel.add(invokeHelpButton(), BorderLayout.EAST);
    }

    private JPanel createCards(){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6,6));
        ImageIcon openedCard;
        ImageIcon closedCard = new ImageIcon("D:\\Kuliah\\Term_2\\DDP2\\DDP2_TP\\TP_DDP_2\\assignment-4\\src\\main\\java\\frontCard.png");
        closedCard = resizeIcon(closedCard);
        int j = 1;
        for (int i = 1; i <= 36; i++) {
            if (j > 18) j=1;
            JButton button = new JButton();
            button.addActionListener(command);
            openedCard = new ImageIcon("D:\\Kuliah\\Term_2\\DDP2\\DDP2_TP\\TP_DDP_2\\assignment-4\\src\\main\\java\\" + j + ".png");
            openedCard = resizeIcon(openedCard);
            Card temp = new Card(j, button, openedCard, closedCard);
            cardList.add(temp);
            j++;
        }
        shuffleCard();

        for (JButton buttons : Card.buttonList) {
            panel.add(buttons);
        }
        return panel;
    }

    private JPanel invokeHelpButton(){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        JButton restartButton = new JButton("Restart");
        restartButton.addActionListener(e -> restartGame());
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(e -> System.exit(0));
        panel.add(restartButton);
        panel.add(exitButton);
        return panel;
    }
    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void addCounter(){
        tryLabel.setText("Number of tries: " + ++totalCounter);
    }

    public void setLowestRecord(){
        if (lowestRecord == 0) lowestRecord = totalCounter;
        else{
            if (totalCounter < lowestRecord) lowestRecord = totalCounter;
        }
        highLabel.setText("         Current lowest record: " +  lowestRecord);
    }

    private void resetCounter() {
        totalCounter = 0;
        tryLabel.setText("Number of tries: " + totalCounter);
    }

    public Card findCard(JButton button){
        for (Card c : cardList){
            if (button == c.getButton()){
                return c;
            }
        }
        return null;
    }

    private void restartGame(){
        int restart = JOptionPane.showConfirmDialog(mainPanel, "Are you sure to restart the game?", "Restart", JOptionPane.YES_NO_OPTION);
        if (restart == JOptionPane.YES_OPTION){
            mainPanel.remove(buttonPanel);
            mainPanel.revalidate();
            mainPanel.repaint();
            cardList.clear();
            Card.buttonList.clear();
            mainPanel.add(createCards(), BorderLayout.PAGE_START);
            resetStepCounter();
            resetCounter();
        }
    }

    private void shuffleCard(){
        Collections.shuffle(Card.buttonList);
    }

    public void win(){
        setLowestRecord();
        JOptionPane.showMessageDialog(mainPanel, "           CONGRATULATIONS!!\nYOU WON THE GAME WITH " + totalCounter + " STEPS", "CONGRATULATIONS!", JOptionPane.INFORMATION_MESSAGE);
    }

    private ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    public int getStepCounter() {
        return stepCounter;
    }

    public void addStepCounter(){
        stepCounter++;
    }

    public void resetStepCounter(){
        stepCounter = 0;
    }

    public ArrayList<Card> getcardList() {
        return cardList;
    }
}
