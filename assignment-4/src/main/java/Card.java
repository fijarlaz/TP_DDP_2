
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Card {
    private int id;
    private JButton button;
    private ImageIcon openedImage;
    private final ImageIcon closedImage;
    private boolean clicked;
    private boolean correct;
    public static ArrayList<JButton> buttonList = new ArrayList<>();

    public Card(int id, JButton button, ImageIcon openedImage, ImageIcon closedImage){
        this.id = id;
        this.button = button;
        this.openedImage = openedImage;
        this.closedImage = closedImage;
        this.clicked = false;
        buttonList.add(this.button);
        setImage();
    }

    public JButton getButton() {
        return button;
    }

    public int getId() {
        return id;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public void setImage(){
        button.setPreferredSize(new Dimension(150,150));
        if (clicked) button.setIcon(openedImage);
        else{button.setIcon(closedImage);}
    }

    public boolean equals(Card nextCard) {
        return this.getId() == nextCard.getId();
    }

    public void setClicked(boolean clicked){
        this.clicked = clicked;
    }

    public void openCard(){
        button.setIcon(openedImage);
    }

    public void closeCard(){
        button.setIcon(closedImage);
    }

    public boolean isClicked() {
        return clicked;
    }

    public boolean isCorrect() {
        return correct;
    }
}
