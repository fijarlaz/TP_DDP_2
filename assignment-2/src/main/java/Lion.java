class Lion extends Animal{
    private String nama;
    private int panjang;


    public Lion(String nama, int panjang){
        super(nama, panjang);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return super.getNama();
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return super.getPanjang();
    }

    public void hunt(){
        System.out.println("Err....");
    }

    public void goodMood(){
        System.out.println("Hauhhmm!");
    }

    public void badMood(){
        System.out.println("HAAHUM!!");
    }
}
