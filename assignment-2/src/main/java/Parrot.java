class Parrot extends Animal{

    private String nama;
    private int panjang;

    public Parrot(String nama, int panjang){
        super(nama, panjang);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return super.getNama();
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return super.getPanjang();
    }

    public void fly(){
        System.out.println("TERBAAANG...");
    }

    public void talk(String ucapan){
        System.out.println(ucapan.toUpperCase());
    }

    public void nothing(){
        System.out.println("HM?");
    }
}
