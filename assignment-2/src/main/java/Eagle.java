class Eagle extends Animal{
    private String nama;
    private int panjang;


    public Eagle(String nama, int panjang){
        super(nama, panjang);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return super.getNama();
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return super.getPanjang();
    }

    public void cuddled(){
        //attack
    }

    public void fly(){
        System.out.println("Kwaakk....");
    }
}
