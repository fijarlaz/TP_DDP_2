import java.util.ArrayList;
class Cage{
    private String type;
    private String location;
    private Animal animal;
    public Cage(Animal animal){
        this.animal = animal;
        if (this.animal.getNama() == "eagle" || this.animal.getNama() == "lion"){
            this.location = "outdoor";
        }
        else {
            this.location = "indoor";
        }
        if (this.location == "outdoor") {
            if (this.animal.getPanjang() < 75) {
                this.type = "A";
            }
            else if (this.animal.getPanjang() < 90) {
                this.type = "B";
            }
            else {
                this.type = "C";
            }
        }
        else {
            if (this.animal.getPanjang() < 45) {
                this.type = "A";
            }
            else if (this.animal.getPanjang() < 60) {
                this.type = "B";
            }
            else {
                this.type = "C";
            }
        }

    }

    public String getType(){
        return type;
    }
    public void setType(String ype){
        this.type = type;
    }
    public String getLocation(){
        return location;
    }
    public void setLocation(String location){
        this.location = location;
    }

    public Animal getAnimal(){
        return animal;
    }

    public void setAnimal(Animal animal){
        this.animal = animal;
    }
}
