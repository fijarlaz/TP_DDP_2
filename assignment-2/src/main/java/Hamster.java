class Hamster extends Animal{
    private String nama;
    private int panjang;


    public Hamster(String nama, int panjang){
        super(nama, panjang);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return super.getNama();
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return super.getPanjang();
    }

    public void gnawing(){
        System.out.println("Ngkkrit.. Ngkkrrriiit");
    }

    public void runInWheel(){
        System.out.println("Trrr... Trrr...");
    }

}
