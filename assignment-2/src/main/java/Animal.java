public class Animal{
    private String nama;
    private int panjang;

    public Animal(String nama, int panjang){
        this.nama = nama;
        this.panjang = panjang;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return nama;
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return panjang;
    }

    
}
