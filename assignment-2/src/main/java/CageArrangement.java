//package main.java.Cage;

//import main.java.Animal.Animal;
import java.util.ArrayList;

public class CageArrangement{ //Membagi level tiap hewan, jadi perlu panggil arrangeCAge sama reArrangeCage tiap list hewan
    static private ArrayList<Cage> level1 = new ArrayList<Cage>();
    static private ArrayList<Cage> level2 = new ArrayList<Cage>();
    static private ArrayList<Cage> level3 = new ArrayList<Cage>();

    static public void arrangeCage(Cage[] hewan){ //anggap pasti ada TODO
        int jumlahHewan = hewan.length;
        int sisa = jumlahHewan%3;
        int jumlahPerLvl = jumlahHewan/3;

        if(jumlahHewan < 3) {
            switch (jumlahHewan){
                case 0: break;
                case 1:
                    level1.add(hewan[0]); break;
                case 2:
                    level1.add(hewan[0]);
                    level2.add(hewan[1]); break;
            }
        }else{
            for(int i = 0; i < jumlahHewan; i++) {
                level1.add(hewan[i]);
                level2.add(hewan[jumlahPerLvl + i]);
                level3.add(hewan[jumlahPerLvl*2 + i]);
            }

            if(!(sisa==0)){
                switch (sisa){
                    case 1: level3.add(hewan[-1]); break;
                    case 2:
                        level3.add(hewan[-1]);
                        level2.add(hewan[-2]); break;
                }
            }
        }
        System.out.println("Location :" + hewan[0].getLocation());
        printArrangement();
    }

    static public void reArrange(){
        ArrayList<Cage> temp = level3;
        level3 = level2;
        level2 = level1;
        level1 = temp;

        reverse(level1);
        reverse(level2);
        reverse(level3);

        System.out.println("After rearrangement...");
        printArrangement();
    }

    static private ArrayList<Cage> reverse(ArrayList<Cage> level) {
        int length = level.size()-1;
        int lengthDiv2 = length/2;
        Cage temp;
        for(int i = 0; i < lengthDiv2; i++){
            temp = level.get(i);
            level.set(i, level.get(length-i));
            level.set(length-i, temp);
        }
        return level;
    }

    static private void printLevel(ArrayList<Cage> level) {
        Animal hewan;
        for(Cage kandang : level) {
            hewan = kandang.getAnimal();
            System.out.printf(" %s (%d - %s)", hewan.getNama(), hewan.getPanjang(), kandang.getType());
        }
        System.out.println();
    }

    static private void printArrangement() {
        System.out.print("level 3 :");
        printLevel(level3);
        System.out.print("level 2 :");
        printLevel(level2);
        System.out.print("level 1 :");
        printLevel(level1);
    }
}