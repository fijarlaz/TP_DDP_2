import java.util.Scanner;
import java.util.ArrayList;
public class Kebun{
    public static void main(String[] args) {
        Scanner inputNormall = new Scanner(System.in);
        Scanner inputAnimal = new Scanner(System.in); //.useDelimiter("\\,"); //dipisah berdasarkan koma
        String[] paramAnimal;
        String[] listAnimal;
        Animal animal;
        Cage[] arrCat = new Cage[0];
        Cage[] arrLion = new Cage[0];
        Cage[] arrHamster = new Cage[0];
        Cage[] arrEagle = new Cage[0];
        Cage[] arrParrot = new Cage[0];
        int num;
        Cat kucing;
        Lion singa;
        Hamster hamster;
        Eagle elang;
        Parrot kakatua;


        //Mengisi array
        System.out.print("Welcome to Javari Park\n" +
                "Input the number of animals\n" +
                "Cat :");

        num = inputNormall.nextInt();

        if (!(num == 0)) {
            paramAnimal = new String[2];
            arrCat = new Cage[num];
            System.out.println("\nProvide the information of cat(s)");
            listAnimal = inputAnimal.next().split(",");
            for (int i = 0; i < num; i++) {
                paramAnimal = listAnimal[i].split("\\|");
                String param0 = paramAnimal[0];
                int param1 = Integer.parseInt(paramAnimal[1]);
                animal = new Cat(param0, param1);
                arrCat[i] = new Cage(animal);
            }
        }

        System.out.print("Lion :");

        num = inputNormall.nextInt();

        if (!(num == 0)) {
            paramAnimal = new String[2];
            arrCat = new Cage[num];
            System.out.println("\nProvide the information of lion(s)");
            listAnimal = inputAnimal.next().split(",");
            for (int i = 0; i < num; i++) {
                paramAnimal = listAnimal[i].split("//|");
                String param0 = paramAnimal[0];
                int param1 = Integer.parseInt(paramAnimal[1]);
                animal = new Lion(param0, param1);
                arrLion[i] = new Cage(animal);
            }
        }


        System.out.print("Eagle :");
        num = inputNormall.nextInt();

        if (!(num == 0)) {
            paramAnimal = new String[2];
            arrCat = new Cage[num];
            System.out.println("\nProvide the information of eagle(s)");
            listAnimal = inputAnimal.next().split(",");
            for (int i = 0; i < num; i++) {
                paramAnimal = listAnimal[i].split("//|");
                String param0 = paramAnimal[0];
                int param1 = Integer.parseInt(paramAnimal[1]);
                animal = new Eagle(param0, param1);
                arrEagle[i] = new Cage(animal);
            }
        }


        System.out.print("Parrot :");
        num = inputNormall.nextInt();

        if (!(num == 0)) {
            paramAnimal = new String[2];
            arrCat = new Cage[num];
            System.out.println("\nProvide the information of parrot(s)");
            listAnimal = inputAnimal.next().split(",");
            for (int i = 0; i < num; i++) {
                paramAnimal = listAnimal[i].split("//|");
                String param0 = paramAnimal[0];
                int param1 = Integer.parseInt(paramAnimal[1]);
                animal = new Parrot(param0, param1);
                arrParrot[i] = new Cage(animal);
            }
        }

        System.out.print("Hamster :");
        num = inputNormall.nextInt();

        if (!(num == 0)) {
            paramAnimal = new String[2];
            arrCat = new Cage[num];
            System.out.println("\nProvide the information of hamster(s)");
            listAnimal = inputAnimal.next().split(",");
            for (int i = 0; i < num; i++) {
                paramAnimal = listAnimal[i].split("//|");
                String param0 = paramAnimal[0];
                int param1 = Integer.parseInt(paramAnimal[1]);
                animal = new Hamster(param0, param1);
                arrHamster[i] = new Cage(animal);
            }
        }

        System.out.println("Animals have been successfully recorded!\n" +
                "\n" +
                "=============================================\n" +
                "Cage arrangement:");

        if(arrCat.length > 0) {
            CageArrangement.arrangeCage(arrCat);
            CageArrangement.reArrange();
        }

        if(arrHamster.length > 0) {
            CageArrangement.arrangeCage(arrHamster);
            CageArrangement.reArrange();
        }

        if(arrParrot.length > 0) {
            CageArrangement.arrangeCage(arrParrot);
            CageArrangement.reArrange();
        }

        if(arrEagle.length > 0) {
            CageArrangement.arrangeCage(arrEagle);
            CageArrangement.reArrange();
        }

        if(arrLion.length > 0) {
            CageArrangement.arrangeCage(arrLion);
            CageArrangement.reArrange();
        }


        System.out.println("=============================================");

        String searchNama;
        Animal hewan;
        boolean namaKetemu;

        while (true) {
            namaKetemu = false;
            System.out.println("Which animal you want to visit?\n" +
                    "1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            num = inputNormall.nextInt();
            if (num == 1) {
                System.out.println("Mention the name of cat you want to visit: ");
                searchNama = inputNormall.next();
                for (Cage kandang : arrCat) {
                    hewan = kandang.getAnimal();
                    if (hewan.getNama().equals(searchNama)) {
                        kucing = (Cat) hewan;
                        namaKetemu = true;
                        System.out.println("You are visiting " + searchNama + "(cat) now, what would you like to do?\n" +
                                "1: Brush the fur 2: Cuddle");
                        num = inputNormall.nextInt();
                        switch (num) {
                            case 1:
                                kucing.brushTheFur();
                                break;
                            case 2:
                                kucing.cuddleTheCat();
                                break;
                            default:
                                System.out.println("You do nothing...");
                        }
                        break;
                    }
                }
                if (!namaKetemu) {
                    System.out.print("There is no cat with that name");
                }
            } else if (num == 2) { //Eagle
                System.out.println("Mention the name of eagle you want to visit: ");
                searchNama = inputNormall.next();

                for (Cage kandang : arrEagle) {
                    hewan = kandang.getAnimal();
                    if (hewan.getNama().equals(searchNama)) {
                        namaKetemu = true;
                        elang = (Eagle) hewan;
                        System.out.println("You are visiting " + searchNama + "(eagle) now, what would you like to do?\n" +
                                "1: Order to fly");
                        num = inputNormall.nextInt();
                        switch (num) {
                            case 1:
                                elang.fly();
                                break;
                            default:
                                System.out.println("You do nothing...");
                        }
                        break;
                    }
                }
                if (!namaKetemu) { //kalau tidak ada namanya
                    System.out.print("There is no eagle with that name");
                }
            } else if (num == 3) { //Hamster
                System.out.println("Mention the name of hamster you want to visit: ");
                searchNama = inputNormall.next();
                for (Cage kandang : arrHamster) {
                    hewan = kandang.getAnimal();
                    if (hewan.getNama().equals(searchNama)) {
                        namaKetemu = true;
                        hamster = (Hamster) hewan;
                        System.out.println("You are visiting " + searchNama + "(hamster) now, what would you like to do?\n" +
                                "1: See it gnawing 2: Order to run in the hamster wheel");
                        num = inputNormall.nextInt();
                        switch (num) {
                            case 1:
                                hamster.gnawing();
                                break;
                            case 2:
                                hamster.runInWheel();
                                break;
                            default:
                                System.out.println("You do nothing...");
                        }
                        break;
                    }
                }
                if (!namaKetemu) {
                    System.out.print("There is no hamster with that name");
                }
            } else if (num == 4) { //Parrot
                System.out.println("Mention the name of parrot you want to visit: ");
                searchNama = inputNormall.next();
                for (Cage kandang : arrParrot) {
                    hewan = kandang.getAnimal();
                    if (hewan.getNama().equals(searchNama)) {
                        namaKetemu = true;
                        kakatua = (Parrot) hewan;
                        System.out.println("You are visiting " + searchNama + "(parrot) now, what would you like to do?\n" +
                                "1: Order to fly 2: Do conversation");
                        num = inputNormall.nextInt();
                        switch (num) {
                            case 1:
                                kakatua.fly();
                                break;
                            case 2:
                                System.out.println("You say: ");
                                String mimic = inputNormall.nextLine();
                                kakatua.talk(mimic);
                                break;
                            default:
                                kakatua.nothing();
                        }
                        break;
                    }
                }
                if (!namaKetemu) {
                    System.out.print("There is no parrot with that name");
                }

            } else if (num == 5) { //Lioin
                System.out.println("Mention the name of lion you want to visit: ");
                searchNama = inputNormall.next();
                for (Cage kandang : arrLion) {
                    hewan = kandang.getAnimal();
                    if (hewan.getNama().equals(searchNama)) {
                        namaKetemu = true;
                        singa = (Lion) hewan;
                        System.out.println("You are visiting " + searchNama + "(lion) now, what would you like to do?\n" +
                                "1: See it hunting 2: Brush the mane 3: Disturb it");
                        num = inputNormall.nextInt();
                        switch (num) {
                            case 1:
                                singa.hunt();
                                break;
                            case 2:
                                singa.goodMood();
                                break;
                            default:
                                singa.badMood();
                        }
                        break;
                    }
                }
                if (!namaKetemu) {
                    System.out.print("There is no lion with that name");
                }
            }

            else if (num == 99) {
                System.out.print("Selesai berkeliling kebun saatnya ");
                break;
            }

            System.out.println("Back to the office!");
        }
    }
}