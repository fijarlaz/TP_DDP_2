import java.util.Random;
class Cat extends Animal{
    private String nama;
    private int panjang;
    public Cat(String nama, int panjang){
        super(nama, panjang);
    }

    public void brushTheFur(){
        System.out.println("Nyaaan...");
    }

    public void cuddleTheCat(){
        String[] suara = new String[]{"Miaaaw..","Purrr..","Mwaw!","Mraaawr!"};
        int rnd = new Random().nextInt(suara.length);
        System.out.println(suara[rnd]);
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return super.getNama();
    }

    public void setPanjang(int panjang){
        this.panjang = panjang;
    }

    public int getPanjang(){
        return super.getPanjang();
    }

}
