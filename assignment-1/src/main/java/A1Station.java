import java.util.Scanner;

public class A1Station {

  private static final double THRESHOLD = 250; // in kilograms
  // You can add new variables or methods in this class
  public double getTHRESHOLD(){
    return THRESHOLD;
  }
  static TrainCar kereta = null;
  static int jumlahKucing = 0;

  public static void main(String[] args) {

    double panjangKucing;
    double beratKucing;
    String namaKucing;

    Scanner input = new Scanner(System.in);
    String masukan = input.nextLine();
    for (int i = 0;i<Integer.parseInt(masukan);i++ ) {
      String[] info = input.nextLine().split(",");
      namaKucing = info[0];
      beratKucing = Double.parseDouble(info[1]);
      panjangKucing = Double.parseDouble(info[2]);
      WildCat kucing = new WildCat(namaKucing, beratKucing, panjangKucing);
      jumlahKucing++;
      if (kereta == null) {
        kereta = new TrainCar(kucing);
      }
      else {
        kereta = new TrainCar(kucing, kereta);
      }
      if (kereta.computeTotalWeight() >= THRESHOLD) {
        printAll();
      }

    }
    if (kereta != null) {
      printAll();
    }
  }
  public static void printAll(){
    String tipe ="";
    double averageMassIndex = kereta.computeTotalMassIndex() / jumlahKucing;
    System.out.println("The train departs to Javari Park");
    System.out.print("[LOCO]<"); kereta.printCar();

    System.out.format("\nAverage mass index of all cats: %.2f%n", averageMassIndex);
    if (averageMassIndex < 18.5) {
      tipe = "*underweight*";
    }
    else if (averageMassIndex <25) {
      tipe = "*normal*";
    }
    else if (averageMassIndex < 30) {
      tipe = "*overweight*";
    }
    else{
      tipe = "*obese*";
    }
    System.out.println("In average, the cats in the train are "+ tipe);
    kereta = null;
    jumlahKucing=0;
    //berangkat
  }
}
