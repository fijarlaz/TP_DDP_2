public class TrainCar {
  WildCat cat;
  TrainCar next;

  public static final double EMPTY_WEIGHT = 20; // In kilograms

  // TODO Complete me!

  public TrainCar(WildCat cat) {
    this.cat = cat;

      // TODO Complete me!
  }

  public TrainCar(WildCat cat, TrainCar next) {
    this.cat = cat;
    this.next = next;
      // TODO Complete me!
  }

  public double computeTotalWeight() {
      if (this.next == null) {
        return EMPTY_WEIGHT + this.cat.weight;
      }
      return this.cat.weight + EMPTY_WEIGHT + this.next.computeTotalWeight();
  }

  public double computeTotalMassIndex() {
    if (this.next == null) {
      return this.cat.computeMassIndex();
    }
    return this.cat.computeMassIndex() +this.next.computeTotalMassIndex();
  }

  public void printCar() {
    if (this.next == null) {
      System.out.print("--(" +this.cat.name +")");
    }
    else {
      System.out.print("--(" +this.cat.name +")"); this.next.printCar();
    }
  }
}
